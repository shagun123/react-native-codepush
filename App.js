import React from 'react';
import {Text} from 'react-native';
import withCodePush from './codepush';


class App extends React.Component {

    render() {
        return (
            <>
                <Text>
                    hello i am a react native app
                </Text>
                <Text>
                    testing codepush
                </Text>
            </>
        );
    }
}

export default withCodePush(App);
